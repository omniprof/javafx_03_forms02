package com.kenfogel.javafx_03_forms02;

import com.kenfogel.javafx_03_forms02.data.UserBean;
import com.kenfogel.javafx_03_forms02.presentation.Form02GUI;
import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Example of a form styled by css
 *
 * @author Ken
 */
public class MainApp extends Application {

    private Form02GUI gui;
    private UserBean userBean;

    /**
     * Rather than a constructor, a class that extends Application uses an init
     * method.
     */
    @Override
    public void init() {
        userBean = new UserBean();
        gui = new Form02GUI(userBean);
    }

    /**
     * The start method must be overridden in a class that extends Application
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        gui.start(primaryStage);
    }

    /**
     * Where is all begins but this time there is only one line of code.
     *
     * @param args
     */
    public static void main(String[] args) {
        Application.launch(args);
    }
}
